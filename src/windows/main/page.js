/* В ipcRenderer процессе для обращения к дочерним объектам electron нужно использовать electron.remote
   Вместо electron.app - electron.remote.app. Но electron.remote устарел (deprecated).
   Теперь в ipcRenderer используется отдельный модуль @electron/remote
 */
const { app, desktopCapturer,  } = require('@electron/remote');
const log = require('electron-log');
const { spawn, spawnSync } = require('child_process');

require('../../modules/logger/index.js');

const ready = async () => {
    desktopCapturer.getSources({ types: ['window', 'screen'] }).then(async sources => {
        for (const source of sources) {
            log.info('source.name', source);
            if (source.name.indexOf('Entire Screen') >= 0) {
                try {
                    const stream = await navigator.mediaDevices.getUserMedia({
                        audio: false,
                        video: {
                            mandatory: {
                                chromeMediaSource: 'desktop',
                                chromeMediaSourceId: source.id,
                                minWidth: 1280,
                                maxWidth: 1280,
                                minHeight: 720,
                                maxHeight: 720
                            }
                        }
                    })
                    handleStream(stream)
                } catch (e) {
                    handleError(e)
                }
                return
            }
        }
    })

    function handleStream (stream) {
        const video = document.querySelector('video')
        video.srcObject = stream
        video.onloadedmetadata = (e) => video.play()
    }

    function handleError (e) {
        console.log(e)
    }

};

document.addEventListener('DOMContentLoaded', ready);
document.addEventListener('keydown', (event) => {
    if (event.code === 'F5') {
        document.location.reload();
    }
});
