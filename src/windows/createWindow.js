let { BrowserWindow } = require('electron');

if (!BrowserWindow) {
    BrowserWindow = require('@electron/remote').BrowserWindow;
}

let win = null;

const createWindow = async (src = '', options = {}, display = {}) => {
    const { bounds } = display;
    const baseOptions = {
        frame: false,
        resizable: false,
        movable: true,
        webPreferences: {
            contextIsolation: false,
            nodeIntegration: true,
            enableRemoteModule: true,
            experimentalFeatures: false,
            spellcheck: false,
            enableWebSQL: false,
            textAreasAreResizable: false
        }
    };

    if (bounds) {
        baseOptions.width = bounds.width;
        baseOptions.height = bounds.height;
        baseOptions.x = bounds.x;
        baseOptions.y = bounds.y;
    }

    win = new BrowserWindow(Object.assign(baseOptions, options));
    await win.loadFile(src);
    win.on('closed', () => win = null);

    if (bounds) {
        setImmediate(() => win.setBounds(bounds));
    }

    return win;
};

module.exports = createWindow;
