const log = require("electron-log");

//инициализация логера
try {
    log.transports.console.level = 'silly';
    log.transports.console.forceStyles = true;

    log.transports.file = false;
} catch (ex) {
    console.error(ex);
}
