const { app } = require('electron');
const log = require('electron-log');
const path = require("path");
const fs = require("fs");

require('@electron/remote/main').initialize();

if (process.env.DEBUG) {
    process.env.ELECTRON_IS_DEV = "0";
}

require("./modules/logger/index.js");

const createWindow = require('./windows/createWindow.js');

if ( !app.requestSingleInstanceLock() ) {
    app.quit();
}

app.allowRendererProcessReuse = false;

//костыль для решения проблемы userData в режиме отладки
const appName = require('../package.json').name;

app.setName(appName);

const appPath = process.env.PORTABLE_EXECUTABLE_DIR || process.cwd();//Путь к месту запуска приложения

//Создаём при необходимости папки с данными приложения
const userData = path.join(appPath, `${appName}_appData`);

if (!fs.existsSync(userData)) {
    try {
        fs.mkdirSync(userData, { recursive: true });
        log.info('[info] Directory created:', userData);
    } catch (error) {
        log.error(`[error] Can not create directory "${userData}"\nError: ${error.message}`);
        log.error(`\n\nПриложение остановлено!\nне создалась папка для данных приложения.\n\n`);

        return;
    }
}

app.setPath('userData', userData);

app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');
app.commandLine.appendSwitch('force-color-profile', 'srgb');
app.commandLine.appendSwitch('ignore-gpu-blacklist', 'true');

let mainWindow = null;

//создание и отрисовка окна
async function createMainWindow() {
    try {
        const src = path.join(__dirname, 'windows', 'main', "index.html");
        const options = {
            id: 'main',
            frame: true,
            resizable: true,
            width: 800,
            minWidth: 800,
            height: 600,
            minHeight: 600,
            center: true
        };

        mainWindow = await createWindow(src, options);
        mainWindow.on('closed', app.quit);
        mainWindow.webContents.openDevTools();
    } catch (ex) {
        log.error(ex.stack);
    }
}

app.once('ready', createMainWindow);

app.on('window-all-closed', function () {
    try {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    } catch (ex) {
        log.error(ex.stack);
    }
});

app.on('activate', function () {
    try {
        if (mainWindow === null) {
            createMainWindow();
        }
    } catch (ex) {
        log.error(ex.stack);
    }
});

